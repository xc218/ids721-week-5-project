## Ids721 Week 5 Project

### Project Description

Building a serverless Rust Microservice.

Requirements: 
* Create a Rust AWS Lambda function (or app runner)
* Implement a simple service
* Connect to a database

### Rust Setup
* Create a new cargo lambda project
`cargo lambda new mini5`
`cd mini5`

* Add dependencies to `Cargo.toml`

* Edit `main.rs` to add function.
The function in this project is searching for the student with the input of school and program. This function interacts with DynamoDB.

* Build and deploy to AWS
`cargo lambda build --release`
`cargo lambda deploy --region us-east-1 --iam-role arn:aws:iam::767398138914:role/ids721`

### Build DynamoDB Table
* Open `AWS DynamoDB` and `create table` called `studentInfo`
* In `Explore items`, create items using `json` view.
For example,
```
{
  "student_id": {
    "N": "1"
  },
  "name": {
    "S": "Mark"
  },
  "school": {
    "S": "Duke University"
  },
  "student_program": {
    "S": "ECE"
  }
}
```

### Lambda Function SetUp
* In `AWS Lambda`, choosing the lambda function we deployed before.
* `Add trigger` to create a REST API
* Deploy the API

### Lambda Function Test
* Under `AWS Lambda`, create a test event.
![event](image/event.png)
* Test result:
![result](image/result.png)
This shows that the function searches using "Duke University" and "ECE", and successfully found "Mark"